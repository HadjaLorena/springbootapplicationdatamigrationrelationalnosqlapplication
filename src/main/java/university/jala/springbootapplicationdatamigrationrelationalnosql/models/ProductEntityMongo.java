package university.jala.springbootapplicationdatamigrationrelationalnosql.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.Date;

@Document(collection = "products")
@Data
@AllArgsConstructor
public class ProductEntityMongo {

    @MongoId
    private String id;

    @Field(name = "NM_PRODUCT")
    private String name;

    @Field(name = "PC_PRODUCT")
    private Double price;

    @Field(name = "STK_PRODUCTS")
    private Integer stock;

    @Field(name = "DT_CREATED_AT")
    private Date createdAt;

    @Field(name="DT_UPDATE_AT")
    private Date updatedAt;
}
