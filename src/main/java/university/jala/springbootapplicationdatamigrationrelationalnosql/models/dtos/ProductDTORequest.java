package university.jala.springbootapplicationdatamigrationrelationalnosql.models.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record ProductDTORequest(
        @NotNull @NotBlank String name,
        @NotNull Double price,
        @NotNull Integer stock
) {
}
