package university.jala.springbootapplicationdatamigrationrelationalnosql.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import university.jala.springbootapplicationdatamigrationrelationalnosql.models.ProductEntityMongo;

@Repository
public interface ProductRepositoryMongo extends MongoRepository<ProductEntityMongo, String> {
}
