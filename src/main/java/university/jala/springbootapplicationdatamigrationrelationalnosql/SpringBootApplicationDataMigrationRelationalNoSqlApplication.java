package university.jala.springbootapplicationdatamigrationrelationalnosql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApplicationDataMigrationRelationalNoSqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootApplicationDataMigrationRelationalNoSqlApplication.class, args);
    }

}
