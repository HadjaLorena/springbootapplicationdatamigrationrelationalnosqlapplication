package university.jala.springbootapplicationdatamigrationrelationalnosql.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import university.jala.springbootapplicationdatamigrationrelationalnosql.models.ProductEntityMongo;
import university.jala.springbootapplicationdatamigrationrelationalnosql.service.ProductServiceMongo;

import java.util.List;

@RestController
@RequestMapping("/mongo/products")
public class ProductControllerMongo {

    @Autowired
    ProductServiceMongo productServiceMongo;

    @GetMapping
    public ResponseEntity<List<ProductEntityMongo>> getAllProducts() {
        return ResponseEntity.status(HttpStatus.OK).body(productServiceMongo.getAllProducts());
    }
}
