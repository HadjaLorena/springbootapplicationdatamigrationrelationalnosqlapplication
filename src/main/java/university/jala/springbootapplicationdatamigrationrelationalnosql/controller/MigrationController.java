package university.jala.springbootapplicationdatamigrationrelationalnosql.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import university.jala.springbootapplicationdatamigrationrelationalnosql.service.MigrationService;

@RestController
@RequestMapping("/migration")
public class MigrationController {

    @Autowired
    MigrationService migrationService;

    @PostMapping
    public ResponseEntity<String> migrationData(){
        migrationService.migrateProducts();
        return ResponseEntity.status(HttpStatus.OK).body("Migration complete.");
    }
}
