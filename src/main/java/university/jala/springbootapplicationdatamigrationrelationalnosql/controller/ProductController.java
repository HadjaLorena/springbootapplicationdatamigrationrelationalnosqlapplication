package university.jala.springbootapplicationdatamigrationrelationalnosql.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.springbootapplicationdatamigrationrelationalnosql.models.dtos.ProductDTORequest;
import university.jala.springbootapplicationdatamigrationrelationalnosql.models.ProductEntity;
import university.jala.springbootapplicationdatamigrationrelationalnosql.service.ProductService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping
    public ResponseEntity<ProductEntity> createProduct(@RequestBody @Valid ProductDTORequest dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.createProduct(dto));
    }

    @GetMapping
    public ResponseEntity<List<ProductEntity>> getAllProducts() {
        return ResponseEntity.status(HttpStatus.OK).body(productService.getAllProducts());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findProductById(@PathVariable("id") UUID id) {
        Optional<ProductEntity> product = productService.getProductById(id);

        if(product.isEmpty()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");
        return ResponseEntity.status(HttpStatus.OK).body(product.get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProductById(@PathVariable("id") UUID id) {
        boolean success = productService.deleteProduct(id);

        if(success) return ResponseEntity.status(HttpStatus.OK).body("Product deleted successfully");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateProductById(@PathVariable("id") UUID id, @RequestBody @Valid ProductDTORequest dto) {
        ProductEntity updateProduct = productService.updateProduct(dto, id);

        if(updateProduct.equals(null)) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");
        return ResponseEntity.status(HttpStatus.OK).body(updateProduct);
    }
}