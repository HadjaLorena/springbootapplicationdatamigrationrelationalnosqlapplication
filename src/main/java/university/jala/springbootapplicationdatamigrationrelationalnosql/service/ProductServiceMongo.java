package university.jala.springbootapplicationdatamigrationrelationalnosql.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.jala.springbootapplicationdatamigrationrelationalnosql.models.ProductEntityMongo;
import university.jala.springbootapplicationdatamigrationrelationalnosql.repository.ProductRepositoryMongo;

import java.util.List;

@Service
public class ProductServiceMongo {

    @Autowired
    ProductRepositoryMongo productRepositoryMongo;

    public List<ProductEntityMongo> getAllProducts(){
        return productRepositoryMongo.findAll();
    }
}
