package university.jala.springbootapplicationdatamigrationrelationalnosql.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.jala.springbootapplicationdatamigrationrelationalnosql.models.dtos.ProductDTORequest;
import university.jala.springbootapplicationdatamigrationrelationalnosql.models.ProductEntity;
import university.jala.springbootapplicationdatamigrationrelationalnosql.repository.ProductRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public ProductEntity createProduct(ProductDTORequest dto) {
        ProductEntity productEntity = new ProductEntity();
        BeanUtils.copyProperties(dto, productEntity);
        return productRepository.save(productEntity);
    }

    public List<ProductEntity> getAllProducts() {
        return productRepository.findAll();
    }

    public Optional<ProductEntity> getProductById(UUID id) {
        return productRepository.findById(id);
    }

    public Boolean deleteProduct(UUID id) {
        Optional<ProductEntity> productEntity = productRepository.findById(id);

        if(productEntity.isEmpty()) return false;

        productRepository.delete(productEntity.get());
        return true;
    }

    public ProductEntity updateProduct(ProductDTORequest dto, UUID id) {
        Optional<ProductEntity> productEntity = productRepository.findById(id);

        if(productEntity.isEmpty()) return null;

        ProductEntity productEntityToUpdate = productEntity.get();
        BeanUtils.copyProperties(dto, productEntityToUpdate);
        productEntityToUpdate.setUpdatedAt(new Date());
        return productRepository.save(productEntityToUpdate);
    }
}
