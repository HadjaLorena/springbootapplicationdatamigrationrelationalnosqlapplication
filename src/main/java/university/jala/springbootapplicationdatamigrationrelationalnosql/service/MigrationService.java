package university.jala.springbootapplicationdatamigrationrelationalnosql.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.jala.springbootapplicationdatamigrationrelationalnosql.models.ProductEntity;
import university.jala.springbootapplicationdatamigrationrelationalnosql.models.ProductEntityMongo;
import university.jala.springbootapplicationdatamigrationrelationalnosql.repository.ProductRepository;
import university.jala.springbootapplicationdatamigrationrelationalnosql.repository.ProductRepositoryMongo;

import java.util.List;

@Service
public class MigrationService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductRepositoryMongo productRepositoryMongo;

    public void migrateProducts() {
        List<ProductEntity> products = productRepository.findAll();
        for (ProductEntity productEntity : products) {
            ProductEntityMongo productEntityMongo = new ProductEntityMongo(
                    productEntity.getId().toString(),
                    productEntity.getName(),
                    productEntity.getPrice(),
                    productEntity.getStock(),
                    productEntity.getCreatedAt(),
                    productEntity.getUpdatedAt()
            );

            productRepositoryMongo.save(productEntityMongo);
        }
    }
}
